#!/bin/bash
<<COMMENT1
	file to automate the configurations of risefor_light deployment
COMMENT1
echo "

Déploiement en production de l'instant Risefor : "$2"
Sur le nom de domaine : https://"$3

<<COMMENT2
	variables: 
		\$1 -> name of deployment
		\$2 -> name of branch (ie: risefor_light/fr/badbzzz) 
		\$3 -> url of domain (do NOT include https://)
COMMENT2
#pause function to help lisibilitymon
Pause(){
	sleep $1
}

debug='false';
echo "Debug status is "$debug
read -p "
Tapez entrez pour commencer l'installation" -n 1 -r

if [[ $debug = 'false' ]];then
	cd /var/www/; #For server
else
	cd ~/Documents/risefor/appli; #for dev
fi
echo "

L'installation va se faire dans le dossier : 
" $(pwd)"/"$1
read -p "
Tapez 'entrer' pour continuer, pour changer le nom du dossier tapez ctrl+c et adaptez le 1er paramètre de la commande de lancement. " -n 1 -r

git clone https://git.startinblox.com/applications/risefor-front/ $1;
cd $1;
git checkout $2
python3 -m venv env;
source ./env/bin/activate;
pip3 install -r requirements.txt;
echo "Nous allons installer npm et sass, utilisé pour compiler le css"
sudo npm install -g npm
sudo npm install -g sass
git clone https://git.startinblox.com/djangoldp-packages/risefor-lobbying;
cd risefor-lobbying;
cd ../united4earth;
ln -s ../risefor-lobbying/risefor_lobbying/;
cp server/settings_sample.py server/settings.py;

echo "

L'installation des fichiers est finalisé.
Commençons la personnalisation des fichiers de production.
Vous pourrez changer toutes ces informations dans le fichier settings.py

Quel est le nom de votre organisation (sera affiché publiquement) ?"
read ORGNAME

echo "
Les actions seront publiée au nom de :"  $ORGNAME
read -p "Tapez 'o' pour continuer, 'n' pour changer" -n 1 -r
	if [[ $REPLY =~ ^[Nn]$ ]]; then
		echo "Quel est le nom de votre organisation (sera affiché publiquement) ?"
		read ORGNAME
	fi
echo "

Passons sur la coté de la fédération."
read -p "Souhaitez vous activez la Fédération sur votre site ? (o/n)" -n 1 -r
	if [[ $REPLY =~ ^[Oo]$ ]]; then
		FEDERATION_STATUS="True"
	else
		FEDERATION_STATUS="False"
		echo "else"
	fi
echo "
C'est tout pour le côté données. Les fichiers settings.py et packages.yml vont être personnalisés.
"
Pause "3"
settingsfile='./server/settings.py'
tmpSettingsfile=$settingsfile.tmp
### Changes to settings.py
# org name
sed 's/INSTANCE_NAME =.*/INSTANCE_NAME = "'"$ORGNAME"'"/g' $settingsfile > $tmpSettingsfile
mv $tmpSettingsfile $settingsfile
#federation status
sed 's/FEDERATION_ACTIVE =.*/FEDERATION_ACTIVE = '"$FEDERATION_STATUS"'/g' $settingsfile > $tmpSettingsfile
mv $tmpSettingsfile $settingsfile

### Changes to packages.yml
packageFile='packages.yml'
tmpPackageFile=$packageFile.tmp
sed 's/http:\/\/localhost:8000/https:\/\/'"$3"'/g' $packageFile  > $tmpPackageFile;
mv $tmpPackageFile $packageFile;
echo "L'étape de configuration est terminée.
Le nom de votre instance est "$ORGNAME", le statue de la fédération est "$FEDERATION_STATUS
echo "et votre nom de domaine est : https://"$3

read -p "Tapez 'entrez' pour continuer" -n 1 -r
# Pause "2"

#####
# DATA
#####
echo "L'étape de personnalisation de votre site est terminée.

# Nous allons maintenant passé au coté 'données' de votre déploiement."

databaseOptions=("Sqlite3" "Postgresql")
batabaseType=""
PS3="Quel type de base de données souhaitez vous utiliser ?"
select opt in "${databaseOptions[@]}"
do
    case $opt in
        "Sqlite3")
			batabaseType="sqlite3"
            echo "you chose choice 1"
			break
            ;;
        "Postgresql")
			batabaseType="postgresql"
            echo "you chose choice 2"
			break
            ;;
        *) echo "Veuillez choisir l'une des 2 bases de données proposées. En cas d'hésitation, utilisez sqlite3 en local et postgresql en production";;
    esac
done
echo "Database chosen is $batabaseType"
#edit settings file in order to apply db type
settingsfile2='./server/settings.py'
tmpSettingsfile2=$settingsfile.tmp
if [[ $batabaseType == 'sqlite3' ]]; then
	echo "changes for sqlite3"
	sed 's/dataBaseType = .*/dataBaseType = "dev"/g' $settingsfile2 > $tmpSettingsfile2
	mv $tmpSettingsfile2 $settingsfile2
elif [[ $batabaseType == 'postgresql' ]]; then
	echo "changes for postgresql"
	sed 's/dataBaseType = .*/dataBaseType = "postgresql"/g' $settingsfile2 > $tmpSettingsfile2
	echo "Nous allons maintenant paramétrer les informations de votre base de données.
	De la doc est disponible dans le fichier docs/postgresql.txt pour plus d'informations
	"
	mv $tmpSettingsfile2 $settingsfile2
	echo "Entrez le noms de votre base de données"
	read DB_NAME
	sed "s/'NAME': 'database_name'/'NAME': '"$DB_NAME"'/g" $settingsfile2 > $tmpSettingsfile2
	mv $tmpSettingsfile2 $settingsfile2
	echo "Entrez le noms d'utilisateur"
	read DB_USER
	sed "s/'USER': 'user_name'/'USER': '"$DB_USER"'/g" $settingsfile2 > $tmpSettingsfile2
	mv $tmpSettingsfile2 $settingsfile2
	echo "Entrez le mot de passe"
	read DB_PASS
	ESCAPED_REPLACE=$(printf '%s\n' "$DB_PASS" | sed -e 's/[\/&]/\\&/g')
	sed "s/'PASSWORD': 'user_password'/'PASSWORD': '"$ESCAPED_REPLACE"'/g" $settingsfile2 > $tmpSettingsfile2
	mv $tmpSettingsfile2 $settingsfile2
	
	# Placing hotsname as localhost by default
	echo "We are setting the 'hostname' to 'localhost' by default, you can change this in the backend"
	sed "s/'HOST': 'backend_url'/'HOST': 'localhost'/g" $settingsfile2 > $tmpSettingsfile2
	mv $tmpSettingsfile2 $settingsfile2

fi
echo "Installing 'gettext'"
sudo apt-get install gettext;
echo "Les informations nécessaires vont maintenant être chargé automatiquement, "
Pause "5"
python3 manage.py compilemessages;
python3 manage.py makemigrations;
python3 manage.py migrate;
python3 manage.py loaddata representativeFunctions;
python3 manage.py loaddata organisationTypes;
python3 manage.py loaddata countries;
python3 manage.py loaddata newsletterOptions;
python3 manage.py loaddata installationHelp;
python3 manage.py loaddata contactPlateforms;
python3 manage.py loaddata contactReasons;

echo "

Passons à l'étape 'action' de votre déploiement
Pour commencer, nous allons charger automatiquement votre nom de domaine dans les sources de données.
Vous pouvez voir et modifier cette information dans la section 'ldpsources' de l'admin.

C'est aussi dans cette section que vous pourrez ajouter les sources de données d'autres instances de Risefor. Cela vous permettra d'afficher leurs actions, ou utiliser leur base de données d'élues.

Bienvenue dans le monde 'inter-opérable', grâce à la technologie Startin'Blox :)
"
read -p "Tapez 'entrez' pour continuer" -n 1 -r
########
# Actions info
########
localActionFixture='./risefor_lobbying/fixtures/localActionSource.json';
localActionFixtureTmp=$localActionFixture.tmp
sed 's/localSiteDomaine/https:\/\/'$3'/g' $localActionFixture > $localActionFixtureTmp
mv $localActionFixtureTmp $localActionFixture
python3 manage.py loaddata $localActionFixture

if [[ $FEDERATION_STATUS="True" ]]; then
	read -p "
Souhaitez vous afficher les actions de https://agir.risefor.org ? (o/n)" -n 1 -r
	if [[ $REPLY =~ ^[Oo]$ ]]; then
		python3 manage.py loaddata risefor_lobbying/fixtures/federatedActionSources.json;
	fi
fi

read -p "
Souhaitez vous charger des actions exemple ? (o/n)" -n 1 -r
	if [[ $REPLY =~ ^[Oo]$ ]]; then
		python3 manage.py loaddata actionExample;
	fi

### Update ownership of files
if [[ $debug = 'false' ]];then
	cd ../../;
	echo "Mise à jour des permission du dossier : "$1
	sudo chown www-data:www-data -R $1;
	cd -;
fi
# cd ../../;
# echo "DEBUG: Mise à jour des permission du dossier : "$1
# # sudo chown www-data:www-data -R $1;
# cd -;

echo "

Information Risefor : 
Si vous souhaitez rendre vos actions visible sur https://agir.risefor.org envoyez nous un email avec votre nom de domaine, nous l'ajouterons dans les sources de données :)
"
read -p "Tapez 'entrer' pour continuer" -n 1 -r


#####
# Reprensentative info
#####
# echo "Chargement des données par default réussi.

# Etape données des élu.e.s :)
# Vous avez 3 possibilités :
# 	1. Charger votre propre fichier de données. Dans ce cas, ne chargez rien pour l'instant et rendez vous dans la section d'administration du site (https://votresite.fr/admin/)
# 	2. Charger les fichiers de données que risefor met à votre disposition
# 	3. Utiliser la fonctionnalité d'inter-opérabilité en utilisant la données d'un autre site. Dans ce cas, nous vous suggérons le site de voxpublic.org car l'équipe maintain et améliore les informations régulièrement.
# "


## TODO : Depending on answser, lauch related function

#Function 'Risefor data'
# read -p "
# Load all representatives ?" -n 1 -r
# 	if [[ $REPLY =~ ^[Yy]$ ]]; then
# 		python3 manage.py import_elected --filename 'fr-deputee-prod';
# 	fi
# read -p "
# Load 10 representatives?" -n 1 -r
# 	if [[ $REPLY =~ ^[Yy]$ ]]; then
# 		python3 manage.py import_elected --filename 'fr-deputee-prod' --dev;
# 	fi


echo "La personnalisation de votre instance est terminé."
read -p "Souhaitez vous lancer la configuration serveur ? (o/n)" -n 1 -r
if [[ $REPLY =~ ^[Oo]$ ]]; then
	echo "risefor deployed, close env"
	deactivate;
	echo "Configuration uwsgi conf"
	cd /etc/uwsgi/apps-available/;
	cp sample.ini $1.ini;
	cd ../;
	ln apps-available/$1.ini apps-enabled/$1.ini
	echo "start nginx conf"
	cd /etc/nginx/sites-available/;
	cp sample.conf $1.conf.in;
	sed 's/justicejulie/'$1'/g' $1.conf.in > $1.conf.in2;
	sed 's/julie.risefor.org/'$3'/g' $1.conf.in2 > $1.conf;
	rm $1.conf.in;
	rm $1.conf.in2;
	cd ../;
	ln sites-available/$1.conf sites-enabled/;
	echo "restart services"
	service uwsgi restart;
	nginx -t;
	service nginx restart;
	echo "handle HTTPS certificate"
	sudo  certbot --nginx;
	cd /var/www/$1;
fi

echo "Votre instance Risefor est prête ! :)
Avant de finir, créons votre compte admin"
Pause "2"
cd /var/www/$1/united4earth/
python3 manage.py createsuperuser
echo "C'est fini !
Votre site est accessible sur : https://"$3
Pause "3"
