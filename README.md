# Risefor deployment and server configuration

Within these files you have all you need to deploy Risefor, locally and on a production server.

In order to use these files on your computer or server:
1. Open your terminal
2. Navigate where you wish to install them
3. Type the command `git clone https://gitlab.com/maxime_senza/risefor-server-configs`

## Install system requirements
Launch installation of needed packages in order to run the installation script.

You can install these by running following command : 
`xargs sudo apt-get install -y <requirements.txt`

Make sure you are in this repo when running this command. 

:warning: Python3.9 is installed within this step, if you fear side effects on your machine, use a virtual environment 

## Process to deploy your instancerisefor
_Personnalizing Risefor_
1. Use local deployment script `localDeployment.sh` to personnalise the tool
2. Push your changes to the risefor repo https://git.startinblox.com/applications/risefor-front.

The deployment script will have created the new branch name. You can use `git status` or `git branch -a` to check it, then `git push origin _branch name_` to push the changes

_Publishing your version of Risefor_

3. Log on your server 
4. Run the server deployment script `deployNewInstance.sh` with the needed parameters

## Local deployment - Personnalizing your instance
This file will enable you to install Risefor on your computer, create your own branch and then personnalize the tool.

To start the process :
1. Open your terminal
2. Navigate to the place where you have cloned this repository (ex `cd /home/user/Documents/risefor-server-configs`)
3. Launch the local deployment file by using `. localDeployment.sh` 
4. Follow the installation steps. You may need to run this as sudo 

When you are finished, you need to push your changes to the official repository, to do so:
1. Add and commit your changes (`git add .` & `git commit -m "Explain your changes"`)
2. Push your changes by using `git push origin` + the name of your branch.
If you are unsure of the name of your branch, you can discover it using `git branch -a`, it will be highlighed in green and with a "*" in front of it

## Server deployment - Publishing your instance to the server
This file is to be used on your server.
Server side requirements:
1. Nginx
2. Uwsgi

In order to use this script:
1. Clone this repo on your server
2. Navigate to it
3. Run the command `deployNewInstance.sh` with 3 parameters
-> The name of your deployment, with no spaces. For example: extinction-rebellion
-> The name of your branch, set during the previou step.
-> The Url of the domain where the tool will be used, **without** the https://. For example "agir.risefor.org"

For example, to deploy the version visible on https://agir.risefor.org, the command would be `. deployNewInstance.sh risefor risefor_main/master agir.risefor.org`


### Server config
This scrips assumes the use of nginx and uwsgi on the server.

:warning: make sure you install the `uwsgi-plugin-python3` and that is it accessible to all users.:warning:

### File placement
The file `sample.conf.nginx` must be placed in the folder `/etc/nginx/sites-available/` of the server

The file `sample.conf.uwsgi` must be placed in the folder `/etc/uwsgi/app/apps-available/` of the server


## Script information
Details of each scripts, and the needed parameters, are in the opening comments of each `.sh` files 


## More info
You can contact community @ risefor.org if you have any questions, we're here to help :) 
