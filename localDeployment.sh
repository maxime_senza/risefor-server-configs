#!/bin/bash
<<COMMENT1
	file to automate the configurations of risefor_light deployment
COMMENT1
echo "new risefor deployment"

#pause function to help lisibilitymon
Pause(){
	sleep $1
}

echo "Quel est le nom de votre organisation ?"
read ORGNAME
originalBranch='risefor_main/master'

OrgNameNoSpaces="${ORGNAME// /-}"

echo ${ORGNAME,,} $OrgNameNoSpaces $originalBranch
Pause '5'

# start installation
cd ~/Documents/risefor/appli;
#git clone git@git.startinblox.com:applications/risefor-front.git ${OrgNameNoSpaces,,};
git clone https://git.startinblox.com/applications/risefor-front  ${OrgNameNoSpaces,,};
cd ${OrgNameNoSpaces,,};
git checkout $originalBranch;
git checkout -b 'risefor_instance/'${OrgNameNoSpaces,,};
python3 -m venv env;
source ./env/bin/activate;
pip3 install -r requirements.txt;
echo "Nous allons installer npm et sass, utilisé pour compiler le css"
	# sudo npm install npm
	# sudo npm install sass
#git clone git@git.startinblox.com:djangoldp-packages/risefor-lobbying.git;
git clone https://git.startinblox.com/djangoldp-packages/risefor-lobbying
# cd risefor-lobbying;
# git checkout feature/federation; cd ../;
cd ./united4earth;
ln -s ../risefor-lobbying/risefor_lobbying/;
cp server/settings_sample.py server/settings.py

#start customisation
echo "L'installation des fichiers est finalisé.
Commençons la personnalisation de Risefor pour votre collectif"

echo 'Quel est le nom de domaine de votre site? (incluez le "https://")'
read SITE_NAME

echo "Les actions seront publiée au nom de :"  $ORGNAME
echo "Le site web est :" $SITE_NAME

echo "Passons sur la coté de la fédération. Vous pourrez changer cette information dans votre fichier settings.py"
read -p "Souhaitez vous activez la Fédération sur votre site ? (oui/non)" -n 1 -r
	if [[ $REPLY =~ ^[Oo]$ ]]; then
		FEDERATION_STATUS="True"
	else
		FEDERATION_STATUS="False"
		echo "else"
	fi
echo "
C'est tout pour le côté données. Les fichiers settings.py et packages.yml vont être personnalisés."
settingsfile='./server/settings.py'
tmpSettingsfile=$settingsfile.tmp
### Changes to settings.py
# org name
sed 's/INSTANCE_NAME =.*/INSTANCE_NAME = "'"$ORGNAME"'"/g' $settingsfile > $tmpSettingsfile
mv $tmpSettingsfile $settingsfile
#federation status
sed 's/FEDERATION_ACTIVE =.*/FEDERATION_ACTIVE = '"$FEDERATION_STATUS"'/g' $settingsfile > $tmpSettingsfile
mv $tmpSettingsfile $settingsfile
# No changes to packages.yml on localhost
echo "L'étape de configuration est terminée."
Pause "2"


############ 
# COLORS
############
echo '
Nous allons maintenant personnaliser les couleurs du site,

Vous avez la possibilité de changer les 5 couleurs principales
Vous pouvez utiliser un code hex,rgb ou rgba pour votre couleur.

Pour voir les couleurs par défault vous pouvez visiter https://agir.risefor.org ou laisser les champs vides.

Vous pourrez mettre à jour toutes les couleurs dans le ficher /static/scss/src/_variables.scss

Laissez le champ vide pour garder la couleur par default 
'
echo "Couleur 1 - Utilisé sur la page des actions, notamment sur le bandeaux d'interpellation et les boutons de partages"
read COULEUR1
echo "Entrez la couleur 2 - Utilisé sur le menu, la bar du haut de page ainsi que le pied de page"
read COULEUR2
echo "Entrez la couleur 3 - Utilisez pour les liens et le menu"
read COULEUR3
echo "Couleurs des boutons principaux"
read COULEURCTA
echo "Couleurs des boutons secondaire"
read COULEURCTA2


colorFile='./static/scss/src/_variables.scss'
ChangedColorFile=$colorFile.tmp
if [[ $COULEUR1 != '' ]]; then
	sed 's/$main-color:.*;/$main-color: '$COULEUR1';/g' $colorFile > $ChangedColorFile
	mv $ChangedColorFile $colorFile
fi
if [[ $COULEUR2 != '' ]]; then
	sed 's/$second-color:.*;/$second-color: '$COULEUR2';/g' $colorFile > $ChangedColorFile
	mv $ChangedColorFile $colorFile
fi
if [[ $COULEUR3 != '' ]]; then
	sed 's/$extra-color:.*;/$extra-color: '$COULEUR3';/g' $colorFile > $ChangedColorFile
	mv $ChangedColorFile $colorFile
fi
if [[ $COULEURCTA != '' ]]; then
	sed 's/$main-cta-color:.*;/$main-cta-color: '$COULEURCTA';/g' $colorFile > $ChangedColorFile
	mv $ChangedColorFile $colorFile
fi
if [[ $COULEURCTA2 != '' ]]; then
	sed 's/$third-cta-color:.*;/$third-cta-color: '$COULEURCTA2';/g' $colorFile > $ChangedColorFile
	mv $ChangedColorFile $colorFile
fi

echo "Les nouvelles couleurs sont : " $COULEUR1 $COULEUR2 $COULEUR3 $COULEURCTA $COULEURCTA2

#font familly
echo "Personnalisation des fonts:
Instructions : 
- Incluez l'extension du fichier (.tff/.otf)
- N'incluez pas le nom de la variations, 

Par exemple pour la font 'Helvetica'
tapez 'Helvetica.ttf' et non pas 'Helvetica Bold'.

Après l'installations, incluez vos fichiers de font directement dans le dossiers /united4earth/static/fonts*
"
pause "5"
echo "Vos fonts, laissez vide par default."
echo "La font de vos titres"
read FONT_TITLES

echo "Entrez le nom de la font pour les contenus"
read FONT_BODY

fontFile='./static/scss/src/_fonts.scss'
variablesFile='./static/scss/src/_variables.scss'
tmpFontFile=$fontFile.tmp
tmpVariablesFile=$variablesFile.tmp

#font title changes
if [[ $FONT_TITLES != '' ]]; then
	sed "s/font-stack-1: '.*'/font-stack-1: '"$FONT_TITLES"'/g" $variablesFile > $tmpVariablesFile
	mv $tmpVariablesFile $variablesFile
	sed "s/Helvetica/"$FONT_TITLES"/g" $fontFile > $tmpFontFile
	mv $tmpFontFile $fontFile
fi
#font file changes
if [[ $FONT_BODY != '' ]]; then
	sed "s/font-stack-2: '.*'/font-stack-2: '"$FONT_BODY"'/g" $variablesFile > $tmpVariablesFile
	mv $tmpVariablesFile $variablesFile
	sed "s/Segoe UI/"$FONT_BODY"/g" $fontFile > $tmpFontFile
	mv $tmpFontFile $fontFile
	sed "s/Segoe-UI/"$FONT_BODY"/g" $fontFile > $tmpFontFile
	mv $tmpFontFile $fontFile
fi
#variables file changes


echo "Nous allons mettre à jour le fichier avec ces couleurs"
Pause "5"
#compile new colors
sass static/scss/index.scss static/css/united4earth.css


#####
# DATA
#####
echo "L'étape de personnalisation de votre site est terminée.

# Nous allons maintenant passé au coté 'données' de votre déploiement."

databaseOptions=("Sqlite3" "Postgresql")
batabaseType=""
PS3="Quel type de base de données souhaitez vous utiliser ?"
select opt in "${databaseOptions[@]}"
do
    case $opt in
        "Sqlite3")
			batabaseType="sqlite3"
            echo "you chose choice 1"
			break
            ;;
        "Postgresql")
			batabaseType="postgresql"
            echo "you chose choice 2"
			break
            ;;
        *) echo "Veuillez choisir l'une des 2 bases de données proposées. En cas d'hésitation, utilisez sqlite3 en local et postgresql en production";;
    esac
done
echo "Database chosen is $batabaseType"
#edit settings file in order to apply db type
settingsfile2='./server/settings.py'
tmpSettingsfile2=$settingsfile.tmp
if [[ $batabaseType == 'sqlite3' ]]; then
	echo "changes for sqlite3"
	sed 's/dataBaseType = .*/dataBaseType = "dev"/g' $settingsfile2 > $tmpSettingsfile2
	mv $tmpSettingsfile2 $settingsfile2
elif [[ $batabaseType == 'postgresql' ]]; then
	echo "changes for postgresql"
	sed 's/dataBaseType = .*/dataBaseType = "postgresql"/g' $settingsfile2 > $tmpSettingsfile2
	echo "Nous allons maintenant paramétrer les informations de votre base de données.
	De la doc est disponible dans le fichier docs/postgresql.txt pour plus d'informations
	"
	mv $tmpSettingsfile2 $settingsfile2
	echo "Entrez le noms de votre base de données"
	read DB_NAME
	sed "s/'NAME': 'database_name'/'NAME': '"$DB_NAME"'/g" $settingsfile2 > $tmpSettingsfile2
	mv $tmpSettingsfile2 $settingsfile2
	echo "Entrez le noms d'utilisateur"
	read DB_USER
	sed "s/'USER': 'user_name'/'USER': '"$DB_USER"'/g" $settingsfile2 > $tmpSettingsfile2
	mv $tmpSettingsfile2 $settingsfile2
	echo "Entrez le mot de passe"
	read DB_PASS
	ESCAPED_REPLACE=$(printf '%s\n' "$DB_PASS" | sed -e 's/[\/&]/\\&/g')
	sed "s/'PASSWORD': 'user_password'/'PASSWORD': '"$ESCAPED_REPLACE"'/g" $settingsfile2 > $tmpSettingsfile2
	mv $tmpSettingsfile2 $settingsfile2
	
	# Placing hotsname as localhost by default
	echo "We are setting the 'hostname' to 'localhost' by default, you can change this in the backend"
	sed "s/'HOST': 'backend_url'/'HOST': 'localhost'/g" $settingsfile2 > $tmpSettingsfile2
	mv $tmpSettingsfile2 $settingsfile2

fi
echo "Installing 'gettext'"
sudo apt-get install gettext;
echo "Les informations nécessaires vont maintenant être chargé automatiquement, "
Pause "5"
python3 manage.py compilemessages;
python3 manage.py makemigrations;
python3 manage.py migrate;
python3 manage.py loaddata representativeFunctions;
python3 manage.py loaddata organisationTypes;
python3 manage.py loaddata countries;
python3 manage.py loaddata newsletterOptions;
python3 manage.py loaddata installationHelp;
python3 manage.py loaddata contactPlateforms;
python3 manage.py loaddata contactReasons;

####
# Reprensentative info
####
echo "Chargement des données par default réussi.

Etape données des élu.e.s :)
Vous avez 3 possibilités :
	1. Charger votre propre fichier de données. Dans ce cas, ne chargez rien pour l'instant et rendez vous dans la section d'administration du site (https://votresite.fr/admin/)
	2. Charger les fichiers de données que risefor met à votre disposition
	3. Utiliser la fonctionnalité d'inter-opérabilité en utilisant la données d'un autre site. Dans ce cas, nous vous suggérons le site de voxpublic.org car l'équipe maintain et améliore les informations régulièrement.
"


## TODO : Depending on answser, lauch related function

#Function 'Risefor data'
read -p "Load all representatives?" -n 1 -r
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		python3 manacge.py import_elected --filename 'fr-deputee-prod';
	fi
read -p "Load 10 representatives?" -n 1 -r
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		python3 manage.py import_elected --filename 'fr-deputee-prod' --dev;
	fi


echo "Passon à l'étape 'action' de votre déploiement"
########
# Actions info
########
localActionFixture='./risefor_lobbying/fixtures/localActionSource.json';
localActionFixtureTmp=$localActionFixture.tmp
sed 's/localSiteDomaine/http:\/\/localhost:8000/g' $localActionFixture > $localActionFixtureTmp
mv $localActionFixtureTmp $localActionFixture
python3 manage.py loaddata $localActionFixture

if [[ $FEDERATION_STATUS = "True" ]]; then
	read -p "Souhaitez vous afficher les actions de https://agir.risefor.org ? (o/n)" -n 1 -r
	if [[ $REPLY =~ ^[Oo]$ ]]; then
		python3 manage.py loaddata risefor_lobbying/fixtures/federatedActionSources.json;
	fi
fi

read -p "Souhaitez vous charger des actions exemple ? (o/n)" -n 1 -r
	if [[ $REPLY =~ ^[Oo]$ ]]; then
		python3 manage.py loaddata actionExample;
	fi

# #read -p "Souhaitez vous v" -n 1 -r


echo "Information Risefor : 
Une fois que vous avez lancez votre site,
si vous souhaitez rendre vos actions visible sur https://agir.risefor.org envoyez nous un email avec votre nom de domaine, nous l'ajouterons dans les sources de données :)
"

#####
# Logo info
#####
echo "Personnalisation de votre logo.
Pour personnaliser logo, remplacez le fichier ci dessous:
/united4earth/static/img/Logos/logo.png

Pour personnaliser la favicon, remplacez le fichier ci dessous:
/united4earth/static/img/Favicon/favicon.ico
"
read -p "Tapez entrer pour continuer" -n 1 -r

#####
# Meta title ifo
#####
echo "Personnalisation des titres de pages et autres informations meta.
Pour personnaliser les titres de vos pages, visible sur l'onglet ou quand on partage le site, changez le contenu dans le fichier suivant:
united4earth/templates/head-meta.html
"
read -p "Tapez entrer pour continuer" -n 1 -r

echo "Votre instance Risefor est prête ! :)
Avant de finir, créons votre compte admin"
Pause "4"
python3 manage.py createsuperuser
echo "C'est fini !
Rendez vous sur http://localhost:8000"
Pause "4"
python3 manage.py runserver
